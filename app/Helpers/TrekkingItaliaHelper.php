<?php

namespace TrekkingItalia\Common\Helpers;

use TrekkingItalia\Common\Enums\UserStateEnum;
use TrekkingItalia\Common\Enums\UserStatePartnerEnum;
use TrekkingItalia\Common\Models\Department;
use TrekkingItalia\Common\Models\SiteLog;
use TrekkingItalia\Common\Enums\SiteLogEnum;

class TrekkingItaliaHelper
{
    public static function isDev(): bool
    {
        return env('APP_ENV') == 'staging' || env('APP_ENV') == 'local';
    }

    public static function stateUserArray($field)
    {
        switch ($field) {
            case 'all':
                return ['user_partner' => 1,'user_partner_state_with' => 1];
            case 'non_attivo':
                return ['user_partner' => 1, 'user_partner_state' => UserStatePartnerEnum::REFUSED];
            case 'attivo':
                return ['user_partner' => 1, 'user_partner_state' => UserStatePartnerEnum::ACCEPTED];
            case 'no':
                return ['user_partner_sono_no' => 1];

        }
    }

    public static function stateNews($field)
    {

        switch ($field) {
            case 'all':
                return null;
            case 'pianificata':
                return 'Pianificata';
            case 'pubblicata':
                return 'Pubblicata';
            case 'scaduta':
                return 'Scaduta';
            case 'disattiva':
                return  'Disattiva';
        }
    }
    public static function stateTreks($field)
    {

        switch ($field) {
            case 'all':
                return null;
            case 'pianificata':
                return 'Pianificata';
            case 'pubblicata':
                return 'Pubblicata';
            case 'scaduta':
                return 'Scaduta';
            case 'disattiva':
                return  'Disattiva';
        }
    }
    public static function stateCatalogs($field)
    {

        switch ($field) {
            case 'all':
                return null;
            case 'pianificata':
                return 'Pianificata';
            case 'pubblicata':
                return 'Pubblicata';
            case 'scaduta':
                return 'Scaduta';
            case 'disattiva':
                return  'Disattiva';
        }
    }

    static function cleanPrefixNumber($string)
    {

        $string = preg_replace('/\s+/', '', trim(str_replace('+', '', $string)));

        return $string;
    }

    static function getSettingAdvanceMembership()
    {
        return Department::selectRaw("CONCAT(open_membership_day,'/',open_membership_month) date")->where('id', 1)->first()->date ?? null;
    }

    public static function saveLog($user_id, $func_code, $model_id) {
        if(isset(SiteLogEnum::CODE[$func_code])) {
            $log = new SiteLog();
            $log->user_id = $user_id;
            $log->functionality = $func_code;
            $log->model_type = SiteLogEnum::CODE[$func_code]['model_type'];
            $log->model_id = $model_id;
            $log->save();
            
        } else
            return false;
    }
}

