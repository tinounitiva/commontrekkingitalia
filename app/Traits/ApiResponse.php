<?php


namespace TrekkingItalia\Common\Traits;

trait ApiResponse {

    public function apiRet($data = null, $httpCode = 200, $message = 'Success', $success = 1) {

        $response = [
            'success' => $success,
            'message' => __($message),
            'data' => $data
        ];

        return response()->json($response, $httpCode);
    }
    public function apiRetError($data = null, $httpCode = 200, $message = 'Success', $success =0) {

        $response = [
            'error' => $success,
            'message' => __($message),
            'data' => $data
        ];

        return response()->json($response, $httpCode);
    }
    public function apiRetErrorCustom($data = null, $httpCode = 200, $message = 'Success') {

        $response = [
            'errors' => $data,
            'message' => __($message),
        ];

        return response()->json($response, $httpCode);
    }
}
