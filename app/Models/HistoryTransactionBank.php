<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class HistoryTransactionBank extends Model {

    public $table = 'history_transaction';
    static public $rules = [];
    static public $messages = [];

}
