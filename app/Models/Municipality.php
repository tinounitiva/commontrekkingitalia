<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipality extends Model {
    use SoftDeletes;
    public $table = 'municipalities';
    protected $fillable = [
        'id','zip','name','provinces_id','codice_belfiore','valid_until'
    ];
    static public $rules = [];
    static public $messages = [];

}
