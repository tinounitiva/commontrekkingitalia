<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class State extends Model {

    public $table = 'states';
    protected $fillable = [
        'id','sign','name','sign_iso','sign_alpha','codice_belfiore'
    ];
    static public $rules = [];
    static public $messages = [];

}
