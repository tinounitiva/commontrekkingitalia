<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class ManagerialPosition extends Model {

    public $table = 'managerial_positions';
    protected $fillable = [
        'id','slug','name','active'
    ];
    static public $rules = [];
    static public $messages = [];

}
