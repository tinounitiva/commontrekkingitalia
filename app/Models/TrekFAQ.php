<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekFAQ extends Model {

    public $table = 'treks_faq';
    protected $fillable = [
        'id','trek_id',"data",
        'name','surname',"email",
        'message','parent_id',"answers",
        'created_at','updated_at',"is_visible"
    ];

}

