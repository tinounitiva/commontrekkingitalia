<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model {
    use SoftDeletes;
    public $table = 'departments';

    protected $casts = [
        'region_id' => 'array',
        'subscription_trek_formats' => 'array',
    ];
    protected $fillable = [
        'region_id','slug','name','id','active','province_id','municipality_id','address',"open_membership_month","open_membership_day","cap","open_subscription_day","open_subscription_month","close_subscription_day","close_subscription_month","last_subscription_day","last_subscription_month","subscription_trek_formats","memberships_limit_last_year"
    ];
    static public $rules = [];
    static public $messages = [];

    public function credit_lines(){
        return $this->hasMany('TrekkingItalia\Common\Models\DepartmentCredits');
    }
    public function faxs(){
        return $this->hasMany('TrekkingItalia\Common\Models\DepartmentFaxs');
    }
    public function phone_numbers(){
        return $this->hasMany('TrekkingItalia\Common\Models\DepartmentPhonenumber');
    }
    public function offices(){
        return $this->belongsToMany('TrekkingItalia\Common\Models\DepartmentOffice','department_offices','department_id', 'region_id');
    }
}
