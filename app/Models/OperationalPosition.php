<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class OperationalPosition extends Model {

    public $table = 'operational_positions';
    protected $fillable = [
        'id','slug','name','active'
    ];
    static public $rules = [];
    static public $messages = [];

}
