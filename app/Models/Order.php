<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    public $table = 'orders';

    static public $rules = [];
    static public $messages = [];

}
