<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class SectionMedia extends Model
{

    public $table = 'section_media';

    protected $fillable = [
        'id',
        'section_id',
        'type',
        'title',
        'image_path',
        'image_file_id',
        'image_reference_file_id'

    ];

    static public $rules = [];
    static public $messages = [];
}
