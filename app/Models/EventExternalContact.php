<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class EventExternalContact extends Model {

    public $table = 'events_external_contact';
    protected $fillable = [
        'event_id','id',"email",
        'name','surname','image_path'
    ];

}



