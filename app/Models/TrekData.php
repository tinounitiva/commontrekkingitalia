<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekData extends Model {

    public $table = 'treks_data';
    protected $fillable = [
        'id',
        'trek_id',
        "trek_level_id",
        "note_difficulty", 
        "trek_duration_mm",
        "total_duration_mm",
        'trek_duration_day',
        'trek_duration_hours',
        "scheduled_times",
        "altitude_min_meter",
        "altitude_max_meter",
        "difference_climb",
        'difference_descent',
        'total_route_length_km',
        "total_walking_time_hour",
        "route_type",
        "created_at",
        "updated_at"
    ];

}

