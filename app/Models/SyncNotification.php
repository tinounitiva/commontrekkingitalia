<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class SyncNotification extends Model {

    public $table = 'sync_notifications';
    protected $fillable = [
        'status','params','action','send_email_date','id',"user_id_created"
    ];
    static public $rules = [];
    static public $messages = [];

}
