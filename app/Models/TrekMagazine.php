<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekMagazine extends Model {
    use SoftDeletes;
    public $table = 'trek_magazine';
    protected $fillable = [
        'title','subtitle','id','category_trek_id','department_id',
        'preview_description','template','description','published_at','expiration_at',
        'priority','state','created_at','updated_at','deleted_at','user_id',
        'preview_image_path','preview_image_file_id',"preview_image_reference_file_id",
        'trek_magazine_file_path','trek_magazine_file_path_id','date'
    ];

}
