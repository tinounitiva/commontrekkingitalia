<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class CourseParticipation extends Model {

    public $table = 'courses_participations';
    protected $fillable = [
        'id',
        'course_detail_id',
        'user_id'
    ];

}

