<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class UserMp extends Model {

    public $table = 'user_mp';
    protected $fillable = [
        'managerial_position_id','user_id','id','managerial_area_id','start_date','end_date'
    ];
    static public $rules = [];
    static public $messages = [];

}
