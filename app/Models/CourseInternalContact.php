<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class CourseInternalContact extends Model {

    public $table = 'courses_internal_contact';
    protected $fillable = [
        'course_id','id',"courses_detail_id",
        'courses_detail_is_visible',"user_id","original",
    ];

}



