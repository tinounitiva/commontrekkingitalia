<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class SynAccounting extends Model {

    public $table = 'sync_accounting';
    static public $rules = [];
    static public $messages = [];

}
