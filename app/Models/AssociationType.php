<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class AssociationType extends Model {

    public $table = 'association_types';
    protected $fillable = [
        'value','name','id','active','price','order','type_price'
    ];
    static public $rules = [];
    static public $messages = [];

}
