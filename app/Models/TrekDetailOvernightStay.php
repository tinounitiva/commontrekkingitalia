<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekDetailOvernightStay extends Model {

    public $table = 'treks_detail_overnightstay';
    protected $fillable = [
        'id','trek_daily_program_id',"society","address",
        'phone','email',"end_locality","iban",
        'created_at','updated_at'
    ];

}

