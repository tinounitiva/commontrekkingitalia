<?php


namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentCredits extends Model
{
    use HasFactory;


    protected $fillable = [
       'id', 'department_id', 'name', 'iban','channel_id',"swift","accounting","visible","default_online","default_wallet","default_assoc"
    ];

    public function department(){
        return $this->belongsTo('TrekkingItalia\Common\Models\Department');
    }

}
