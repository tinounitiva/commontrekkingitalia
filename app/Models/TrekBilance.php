<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekBilance extends Model {

    public $table = 'treks_bilance';
    protected $fillable = [
        'id','status',"trek_id",
        'indirect_costs_participants','mermbers_escort',
        'indirect_costs_escort','direct_costs_total',
        'contrigution_indirect_costs','cost_direct_and_indirect',
        'number_partecipants','trek_quote_defined',"trek_quote_proposal",
        'trek_free','note',
    ];

}

