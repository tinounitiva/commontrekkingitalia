<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class CourseDetail extends Model {

    public $table = 'courses_detail';
    protected $fillable = [
        'course_id','name',"date","hours_start","hours_end",
        'subject','place_name',"place_latitude","place_longitude",
        "external_contact_check","internal_contact_check","tutor_check","locality_check"
    ];

}

