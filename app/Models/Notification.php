<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    public $table = 'notifications';

    static public $rules = [];
    static public $messages = [];
    protected $guarded = ["id"];

    protected $fillable = [
      'id',
      'send_notification',
      'user_id_received',
      'object_title',
      'object_details',
      'object_department',
      'object_link',
      'user_link',
      'object_id',
      'user_id_generate',
      'content_text',
      'read',
      'read_at'
    ];


}

