<?php

namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriberReservationsWaiting extends Model
{
    use HasFactory;

    public $table = 'subscriber_reservations_waiting';
    protected $fillable = [
        'id',
        'trek_id',
        'event_id',
        'course_id',
        'type',
        'status',
        'duration',
        'closing_date',
        'complete',
    ];
}
