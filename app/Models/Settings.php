<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class Settings extends Model {

    public $table = 'settings';
    static public $rules = [];
    static public $messages = [];

}
