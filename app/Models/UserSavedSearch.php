<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class UserSavedSearch extends Model {

    public $table = 'users_saved_searches';
    protected $fillable = [
        'id','user_id','name','url'
    ];
    static public $rules = [];
    static public $messages = [];

}
