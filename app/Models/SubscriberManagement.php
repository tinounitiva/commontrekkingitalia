<?php

namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriberManagement extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'subscriber_management';

    protected $fillable = [
        "id",
        "id_row",
        "trek_id",
        "user_id",
        "course_id",
        "event_id",
        "departments_id",
        "format_id",
        "credit_id",
        "date_registration",
        "site_public",
        "user_register",
        "user_partner",
        "quote_request",
        "advance_payment_request",
        "balance_request",
        "quote",
        "refunded",
        "operator",
        "operator_id",
        "deleted_at",
        "paymentFormat",
        "site_public",
        "transaction_id"
    ];
}
