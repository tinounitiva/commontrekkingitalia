<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekEquipment extends Model {

    public $table = 'treks_equipment';
    protected $fillable = [
        'id','treks_note_id',"equipment_id",
        'created_at','updated_at'
    ];

}

