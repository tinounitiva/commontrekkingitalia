<?php


namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentOffice extends Model
{
    use HasFactory;


    protected $fillable = [
        'department_id', 'region_id'
    ];



}
