<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekRoute extends Model {

    public $table = 'treks_route';
    protected $fillable = [
        'id',
        'trek_id',
        "countries",
        'regions',
        'name_start',
        'name_end',
        "latitude_start",
        'longitude_start',
        'latitude_end',
        "longitude_end",
        "locality",
        "longitude_locality",
        "latitude_locality",
        "route_info",
        'created_at',
        'updated_at'
    ];

}


