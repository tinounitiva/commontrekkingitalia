<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class CourseTutor extends Model {

    public $table = 'courses_tutor';
    protected $fillable = [
        'course_id','id',"courses_detail_id",'user_id',
        'courses_detail_is_visible','surname','image_path',"name","email","original",
    ];

}



