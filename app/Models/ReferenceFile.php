<?php


namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReferenceFile extends Model
{
    use HasFactory;


    protected $fillable = [
        'file_id', 'element', 'link','pixel',"dimension"
    ];

    public function file(){
        return $this->belongsTo('TrekkingItalia\Common\Models\FileUpload');
    }

}
