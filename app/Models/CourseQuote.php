<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class CourseQuote extends Model {

    public $table = 'courses_quote';
    protected $fillable = [
        'course_id','id',"course_open_at_types",
        'type_enrollment','registration_methods',"cost",
        'minimum_number_participants','maximum_number_participants',"seats_reserved_venue",
        'registration_closing_date'
    ];

}



