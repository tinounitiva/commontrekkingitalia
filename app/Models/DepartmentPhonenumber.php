<?php


namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentPhonenumber extends Model
{
    use HasFactory;
    protected $fillable = [
        'department_id', 'phone_number', 'label',
    ];

    public function department(){
        return $this->belongsTo('TrekkingItalia\Common\Models\Department');
    }
}
