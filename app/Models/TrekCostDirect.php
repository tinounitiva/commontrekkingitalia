<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekCostDirect extends Model {

    public $table = 'treks_cost_direct';

    protected $fillable = [
        'id','trek_id',"description",
        'subtitle','note',
        'partecipant_amount','escort_amount',
        'group_amount','total',
        'general_expenses','position',"trek_quote_proposal",
        'trek_free','note',"day", "direct_costs_total","descriptionOther","pdcCode"
    ];

}

