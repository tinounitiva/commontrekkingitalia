<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Event extends Model {

    use SoftDeletes;

    public $table = 'events';
    protected $fillable = [
        'title','subtitle','id','date_event','hours_start','hours_end','department_id',
        'is_visible','status','preview_image_path','preview_image_file_id','preview_image_reference_file_id','event_poster_path',
        "event_poster_file_id","event_poster_reference_file_id","short_description","name_location_event",
        "latitude_start","longitude_start","note","struct","quote","complete",'internal_id','is_public'
    ];
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }
}

