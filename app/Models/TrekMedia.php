<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekMedia extends Model {

    public $table = 'treks_media';
    protected $fillable = [
        'id','trek_id',"type","image_path","title","exstension","image_file_id","image_reference_file_id","number_day",
        'created_at','updated_at'
    ];

}

