<?php


namespace TrekkingItalia\Common\Models;

use App\Notifications\ResetPasswordNotification;
use Bavix\Wallet\Interfaces\Wallet;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use TrekkingItalia\Common\Notifications\VerifyEmail;
use Bavix\Wallet\Traits\HasWallet;


class User extends Authenticatable implements MustVerifyEmail, Wallet
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, HasRoles, HasWallet;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'remember_token',
        "username",
        "surname",
        "email",
        "gender",
        "date_birth",
        "country_birth",
        "region_birth",
        "region_domicile",
        "region_residence",
        "city_birth",
        "province_birth",
        "fiscal_code",
        "accept_privacy",
        "accept_newsletter",
        "user_base",
        "user_partner",
        "user_partner_state",
        "user_register_website",
        "expire_at_partner",
        "subscribe_at_partner",
        "phone",
        "cell_phone",
        "country_residence",
        "city_residence",
        "province_residence",
        "address_residence",
        "residence_same_domicile",
        "user_state",
        "country_domicile",
        "city_domicile",
        "province_domicile",
        "address_domicile",
        "profession",
        "degree_study",
        "language",
        "profile_image",
        "document_image",
        "accept_privacy_partner",
        "accept_rule_partner",
        "accept_statute_partner",
        "department_id",
        "profile_reserved",
        "note",
        "document_consent",
        "membership_number",
        "check_fiscal_code",
        "historical_id",
        "newspaper",
        "school_id",
        "materia_insegnamento",
        "gradoScuola_scuola",
        "how_know_trekking_italia",
        "profileVisibility",
        "skill_user",
        "importated_old_db",
        "liferay_user_id",
        "escort_biography",
        "escort_curriculum",
        "enabled_e_wallet",
        "enabled_usage_for_payment",
        "enabled_usage_for_refunded",
        "trek_id",
        "event_id",
        "course_id",
        "e_wallert",
        "accept_terms_ewallet",
        "wish_balance",
        "accountholder",
        "iban",
        "mailup_id",
        "last_login",
        "cap_residence",
        "cap_domicile",
        "privacy_module",
        "created_by",
        "level_id_approvation",
        "accept_privacy_date",
        "accept_privacy_partner_date",
        "accept_rule_partner_date",
        "accept_statute_partner_date",
        "accept_partnerconsent_1",
        "accept_partnerconsent_1_date",
        "accept_partnerconsent_2",
        "accept_partnerconsent_2_date",
        "accept_partnerconsent_3",
        "accept_partnerconsent_3_date",
        "accept_partnerconsent_4",
        "accept_partnerconsent_4_date",
        "disability_type",
        "user_fusion_id"
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {

        $url = env('SITE_PUBLIC') . '/password-reset?token=' . $token;

        $this->notify(new ResetPasswordNotification($url));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }
}
