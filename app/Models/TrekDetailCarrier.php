<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekDetailCarrier extends Model {

    public $table = 'treks_detail_carrier';
    protected $fillable = [
        'id','trek_daily_program_id',"society",
        'address','phone',"email",
        'iban','created_at','updated_at'
    ];

}

