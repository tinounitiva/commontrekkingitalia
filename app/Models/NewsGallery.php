<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class NewsGallery extends Model {

    public $table = 'news_gallery';
    protected $fillable = [
        'image_path','news_id','id','order',"file_id","reference_file_id"
    ];
    static public $rules = [];
    static public $messages = [];
    public function news(){
        return $this->belongsTo('TrekkingItalia\Common\Models\News');
    }
}

