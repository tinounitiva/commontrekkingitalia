<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class JobSyncMailup extends Model {

    protected $table = 'job_sync_mailup';

    protected $casts = ['params' => 'array'];

    protected $hidden = [];

}
