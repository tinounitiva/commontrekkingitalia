<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model {
    use SoftDeletes;
    public $table = 'news';
    protected $fillable = [
        'title','subtitle','id','category_trek_id','department_id','interesting_national','preview_image_path',
        'preview_description','template','cover_path_image_file_id','cover_path_image_reference_file_id','cover_path_video_file_id',
        'cover_path_video_reference_file_id','description','published_at','expiration_at','cover_path_video','cover_path_image','preview_image_path',
        'priority','state','created_at','updated_at','deleted_at','user_id','preview_image_file_id','preview_image_reference_file_id','date'
    ];
    static public $rules = [];
    static public $messages = [];
    public function news_attachments(){
        return $this->hasMany('TrekkingItalia\Common\Models\NewsAttachment');
    }

    public function news_gallery(){
        return $this->hasMany('TrekkingItalia\Common\Models\NewsGallery');
    }
}

