<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class EventQuote extends Model {

    public $table = 'events_quote';
    protected $fillable = [
        'event_id','id',"event_open_at_types",
        'type_enrollment','registration_methods',"cost",
        'minimum_number_participants','maximum_number_participants',"seats_reserved_venue",
        'registration_closing_date'
    ];

}



            