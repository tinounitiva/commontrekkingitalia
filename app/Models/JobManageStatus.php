<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class JobManageStatus extends Model {

    protected $table = 'job_manage_status';

    protected $casts = ['params' => 'array'];

    protected $fillable = [
        'id', 'id_type','model', 'params', 'status', 'errors', 'end_at', 'start_at','last_id_user','tot_users_updated','tot_users_to_update'
    ];
    protected $hidden = [];

}
