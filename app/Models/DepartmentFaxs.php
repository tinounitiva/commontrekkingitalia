<?php


namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentFaxs extends Model
{
    use HasFactory;

    protected $fillable = [
        'department_id', 'label', 'fax',
    ];

    public function department(){
        return $this->belongsTo('TrekkingItalia\Common\Models\Department');
    }
}
