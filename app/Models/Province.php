<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model {
    use SoftDeletes;
    public $table = 'provinces';
    protected $fillable = [
        'region_id','sing','name','id'
    ];
    static public $rules = [];
    static public $messages = [];

}
