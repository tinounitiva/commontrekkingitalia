<?php

namespace TrekkingItalia\Common\Models;


use TrekkingItalia\Common\ModelsReference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Tags\HasTags;

class FileUpload extends Model
{
    use HasTags,SoftDeletes;

    public $table = 'files';
    protected $fillable = [
        'id',
        'title', //titolo immagine scelta dallo user
        'dimension', //
        'pixel',//1920*1080
        "type",//“mp4”, “txt”, “docx”, ecc)
        'parent_id',//“*n_varianti*
        'parent_order',//"posizione variante"
        'region',//
        'municipality',
        'state',
        'active',
        'file',
        'path',
        'uploaded_by',
    ];
    static public $rules = [];
    static public $messages = [];

    public function references()
    {
        return $this->hasMany(ReferenceFile::class, 'file_id', 'id');
    }
}
