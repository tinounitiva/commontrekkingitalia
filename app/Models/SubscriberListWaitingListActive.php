<?php

namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriberListWaitingListActive extends Model
{
    use HasFactory;

    public $table = 'subscriber_list_waiting_list_active';

    protected $casts = [
        'sharing_room' => 'array'
    ];

    protected $fillable = [
        'id',
        'type',
        'status',
        'subscriber_id',
        'subscriber_management_id',
        'user_id',
        'created_by',
        'event_id',
        'course_id',
        'trek_id',
        'user_register',
        'name',
        'lastname',
        'email',
        'cell',
        'date',
        'seat',
        'motivation',
        'operator',
        'note',
        "iscription_by_public_site", "trekSubscriptionId",
        "closing_time",
        "sharing_room",
        "bed_type"
    ];
}
