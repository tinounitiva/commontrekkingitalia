<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class SectionPage extends Model
{
    use SoftDeletes;
    public $table = 'section_pages';

    protected $fillable = [
        'id',
        'title',
        'department_id',
        'status_publication',
        'status',
        'father_page',
        'number_page',
        'template',
        'main_text',
        'subtitle_1',
        'second_text_1',
        'subtitle_2',
        'second_text_2',
        'department_national_type'
    ];

    static public $rules = [];
    static public $messages = [];
}
