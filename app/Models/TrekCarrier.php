<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekCarrier extends Model {

    public $table = 'treks_carrier';
    protected $fillable = [
        'id','treks_route_id',"carrier_id"
    ];

}



