<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;


class Trek extends Model {
    use SoftDeletes;
    public $table = 'treks';

    use HasTranslations;

    public $translatable = ['treks'];

    protected $fillable = [
        'title','subtitle','id','category_trek_id','opening_status','department_id','description','state',
        'created_at','updated_at','deleted_at','user_id','escort_id','format_id',"email",
        "approval_level_id_master","double_active_master","level_of_first_approval_master",
        "approval_level_id_future","double_active_future","level_of_first_approval_future",
        "expiration_at_scheduled","referent_id",'note','status','is_master','date_start',"id_scheduled",
        "date_start_schedulation","id_trek_master",'detail','logistic','description','technical_data','photo','document','daily_program','faq','schedulation','quote','complete',
        "clicked_double_active_master","clicked_double_active_future","label_trek_id",'quote_trek',"category_id","trek_id_old_bd", "importated_old_db",
        'cloned_by','first_schedulation',"organizator_department_id","trek_tipology_id","manchette","path_type","old_id_schedule","trek_code","views","shares","schedulation_number","prepare_update_fields","number_published","replicated_by","internal_id"
    ];
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }
}



