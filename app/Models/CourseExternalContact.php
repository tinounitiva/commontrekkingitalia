<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class CourseExternalContact extends Model {

    public $table = 'courses_external_contact';
    protected $fillable = [
        'course_id','id',"courses_detail_id",
        'courses_detail_is_visible','surname','image_path',"name","email",
        "courses_detail_user_id","original",
    ];

}



