<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekDailyProgram extends Model {

    public $table = 'treks_daily_program';
    protected $fillable = [
        'id','trek_id',"day","day_type",
        'description_day','scheduled_times',"itinerary","overnightstay_ids","carrier_ids","note"
    ];

}

