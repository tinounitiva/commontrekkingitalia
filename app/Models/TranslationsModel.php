<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;


class TranslationsModel extends Model {
    
    public $table = 'translations';

    protected $fillable = [
        'id',
        'key',
        'value',
        'lang_id',
    ];

}



