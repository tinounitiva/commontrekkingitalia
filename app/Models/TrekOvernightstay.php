<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekOvernightstay extends Model {

    public $table = 'treks_overnightstay';
    protected $fillable = [
        'id','treks_route_id',"overnightstay_id",
        'created_at','updated_at'
    ];

}

