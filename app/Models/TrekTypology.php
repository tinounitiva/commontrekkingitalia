<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekTypology extends Model {

    public $table = 'treks_typology';
    protected $fillable = [
        'id','trek_id',"typology_id",
        'created_at','updated_at'
    ];

}

