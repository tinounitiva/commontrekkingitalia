<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class EventVip extends Model {

    public $table = 'events_vip';
    protected $fillable = [
        'event_id','id',"email",
        'name','surname','image_path'
    ];

}



