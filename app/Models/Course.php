<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Course extends Model {

    use SoftDeletes;

    public $table = 'courses';
    protected $fillable = [
        'title','subtitle','id','department_id',
        'is_visible','status','preview_image_path','preview_image_file_id','preview_image_reference_file_id','course_poster_path',
        "course_poster_file_id","course_poster_reference_file_id","short_description","name_location_course",
        "latitude_start","longitude_start","note","struct","quote","complete","detail",'internal_id',"is_public"
    ];
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }
}

