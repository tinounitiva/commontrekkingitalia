<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekEscort extends Model {

    public $table = 'treks_escort';
    protected $fillable = [
        'id','trek_id',"escort_id",
        'created_at','updated_at'
    ];

}

