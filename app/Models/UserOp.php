<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class UserOp extends Model {

    public $table = 'user_op';
    protected $fillable = [
        'operational_position_id','user_id','id','operational_area_id','start_date','end_date'
    ];
    static public $rules = [];
    static public $messages = [];

}
