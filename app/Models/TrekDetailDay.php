<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekDetailDay extends Model {

    public $table = 'treks_detail_day';
    protected $fillable = [
        'id',
        'trek_daily_program_id',
        "start_locality",
        "start_latitude",
        'start_longitude',
        'start_altitude_meter',
        "end_locality",
        "end_latitude",
        "end_longitude",
        "end_altitude_meter",
        'total_route_length_km',
        'total_walking_time',
        "altitude_min_meter",
        "altitude_max_meter",
        "difference_climb",
        "difference_descent",
        "route_type",
        'created_at','updated_at'
    ];

}

