<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parametrization extends Model {
use SoftDeletes;
    public $table = 'parametrizations';
    protected $fillable = [
        'value','iso_code','name','order','active','id','parametres_type','level_approved',
        'price','order','type_price','number_annuities','price_variable','disable_public',"old_id"
    ];
    static public $rules = [];
    static public $messages = [];
}
