<?php

namespace TrekkingItalia\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriberQuote extends Model
{
    use HasFactory;

    public $table = 'subscriber_quote';
    protected $fillable = [
        'id',
        'subscriber_id',
        'quote_request',
        'user_id',
        'trek_quote_id',
        'event_quote_id',
        'course_quote_id',
        'advance_payment_request',
        'balance_request',
    ];

}
