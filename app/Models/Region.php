<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model {
    use SoftDeletes;
    public $table = 'regions';
    protected $fillable = [
        'code', 'name','id'
    ];
    static public $rules = [];
    static public $messages = [];

}
