<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class NewsAttachment extends Model {

    public $table = 'news_attachments';
    protected $fillable = [
        'attachment_path','news_id','id',"file_id","reference_file_id"
    ];
    static public $rules = [];
    static public $messages = [];
    public function news(){
        return $this->belongsTo('TrekkingItalia\Common\Models\News');
    }
}

