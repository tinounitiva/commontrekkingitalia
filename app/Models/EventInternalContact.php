<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class EventInternalContact extends Model {

    public $table = 'events_internal_contact';
    protected $fillable = [
        'event_id','id',"user_id",
    ];

}



