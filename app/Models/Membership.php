<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Membership extends Model {
    use SoftDeletes;
    public $table = 'memberships';
    protected $fillable = [
        'id','association_type_id','price','payment_method_id','operator',
        'date_start','date_end','department_id',"user_id","department_credit_id","channel_id","year_membership","number_annuities","date_advance_setting","paymentCode","transaction_id","operator_id"
    ];
    static public $rules = [];
    static public $messages = [];

}
