<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class UserEscortState extends Model {

    public $table = 'users_escort_state';
    protected $fillable = [
        'id','user_id','data_start','data_end'
    ];
    static public $rules = [];
    static public $messages = [];

}
