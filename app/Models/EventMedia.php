<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class EventMedia extends Model {

    public $table = 'events_media';
    protected $fillable = [
        'event_id','id',
        'type','exstension','image_path','image_file_id',
        'image_reference_file_id'
    ];

}



