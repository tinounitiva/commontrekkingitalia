<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekQuote extends Model {
    public $table = 'treks_quote';
    protected $fillable = [
        'user_id','data_start','id','data_end','date_last_payment','trek_id','closing_date_registration','minimum_participants',
        'maximum_participants','seat_reserved_seats','bookable_seats','places_available','online_registration','date_end_trek_formation',"date_end_last_communication",
        "note","type_quote_participation","minimum_normal_fee",
        "maximum_normal_fee","quota_management","advance_payment_methods","advance_fee","balance_payment_methods","balance_fee",
        "balance_date_end","advance_booking","advance_booking_data_end","quota_reduction_value","note_fee","quote", "custom_quote"
    ];

}

