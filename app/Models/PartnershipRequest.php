<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class PartnershipRequest extends Model {

    public $table = 'partnership_requests';
    protected $fillable = [
        'user_id','department_id','id','date_payment',
        'association_type_id','channel_id','intermediary','department_intermediary_id','info_operator','action_at','status_request'
    ];
    static public $rules = [];
    static public $messages = [];

}
