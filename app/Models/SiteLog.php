<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;

class SiteLog extends Model {

    public $table = 'site_logs';
    
    protected $fillable = [
        'id',
        'user_id',
        'functionality',
        'model_type',
        'model_id',
        'created_at',
        'updated_at'
    ];
}

