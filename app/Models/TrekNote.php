<?php

namespace TrekkingItalia\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TrekNote extends Model {

    public $table = 'treks_note';
    protected $fillable = [
        'id','trek_id',"description",
        'trip_info_start','trip_info_end',"peculiarities_events",
        'created_at','updated_at',"note_seen",
        'note_climate','note_healthcare',"note_various",
    ];

}
