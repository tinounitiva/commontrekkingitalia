<?php

namespace TrekkingItalia\Common\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use Illuminate\Auth\Notifications\VerifyEmail as VerifyEmailBase;
use App\User;
use Illuminate\Support\Facades\App;

class VerifyEmail extends VerifyEmailBase {

    public function toMail($notifiable) {


        $verificationUrl = $this->verificationUrl($notifiable);

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        $sendMail = (new MailMessage)
            ->view('emails.verify', [
                'linkToActive' => $verificationUrl,
            ])
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Verifica email');

        return $sendMail;
    }

}
