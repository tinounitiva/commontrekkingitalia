<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class EventCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {

        $array = [
            'department' => 'required',
            'title' => 'required',
            "visible_public_site" => 'required',
            "date" => 'required',
            "time_start" => 'required',
            "locality" => 'required',
            "longitudine" => 'required',
            "latitudine" => 'required',
        ];

        return $array;

    }

    public function messages()
    {
        return [
            'department.required' => 'Sezione obbligatoria',
            "title.required" => "Titolo obbligatorio",
            "visible_public_site.required" => "Visibilità obbligatorio",
            "date.required" => "Data Evento obbligatorio",
            "locality.required" => "Denominazione della località dell’evento obbligatoria",
            "time_start.required" => "Ora inizio obbligatoria",
            "longitudine.required" => "Longitudine obbligatoria",
            "latitudine.required" => "Latitudine obbligatoria",

        ];
    }
}
