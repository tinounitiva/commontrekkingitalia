<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class TrekTechnicalDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {


        $array = [
            'trek_id' => 'required',
            'difficulty' => 'required',
            'trek_duration_gg' => 'required',
            //'altitude_min' => 'required',
            //'altitude_max' => 'required',
            //'downhill_gradient' => 'required',
            //'uphill_elevation' => 'required',
            //'total_length' => 'required',

        ];


        return $array;

    }

    public function messages()
    {
        return [
            'trek_id.required' => 'Trek è obbligatorio',
            'difficulty.required' => 'Difficoltà è obbligatorio',
            'trek_duration_gg.required' => 'Durata del trek obbligatoria',
            'altitude_min.required' => 'Altitudine minima obbligatoria',
            'altitude_max.required' => 'Altitudine massima è obbligatoria',
            'downhill_gradient.required' => 'Dislivello in discesa obbligatorio',
            'uphill_elevation.required' => 'Dislivello in salita obbligatorio',
            'total_length.required' =>  'Lunghezza complessiva percorso (km) obbligatorio',
            'total_duration.required' => 'Durata complessiva cammino percorso (ore) obbligatorio',
        ];
    }
}
