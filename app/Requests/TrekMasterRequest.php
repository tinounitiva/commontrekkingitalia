<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
class TrekMasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
//    Livello di approvazione scheda master =>approval_level_id_master
//    Doppia approvazione scheda master =>double_active_master
//        Livello di prima approvazione scheda master =>level_of_first_approval_master
//    Livello di approvazione future schedulazioni =>approval_level_id_future
//    Doppia approvazione future schedulazioni =>double_active_future
//        Livello di prima approvazione future schedulazioni=>level_of_first_approval_future
        $array = [
            'title' => 'required',
           // 'subtitle' => 'required',
            'email' => 'required',
            'format' => 'required',
            'type' => 'required',
            'approval_level_id_master' => 'required',
            'approval_level_id_future' => 'required',
        ];


        return $array;
    }

    public function messages()
    {
        return [
            'title.required' => 'Titolo è obbligatorio',
            'subtitle.required' => 'Sottotitolo è obbligatorio',
            'email.required' => 'Email obbligatoria',
            'format.required' => 'Formato obbligatorio',
            'type.required' => 'Tipologia obbligatorio',
            'approval_level_id_master.required' => 'Livello di approvazione scheda master è obbligatorio',
            'approval_level_id_future.required' => 'Livello di approvazione future schedulazion è obbligatorio',
        ];
    }
}
