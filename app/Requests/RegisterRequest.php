<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use TrekkingItalia\Common\Enums\UserStateEnum;
use TrekkingItalia\Common\Services\CodiceFiscaleService;

class RegisterRequest extends FormRequest
{
    protected $cfService;

    public function __construct()
    {
        $this->cfService = new CodiceFiscaleService();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function getCodiceFiscaleDaDati()
    {

        $sesso = $this->get('gender') == 'maschio' ? 'M' : 'F';
        $data_di_nascita = Carbon::parse($this->get('date_birth'))->format('d/m/Y');
        /*$url_check_cf = "http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale?Nome=" . urlencode(strtr($this->get('name'), UserStateEnum::UNWANTEND_CHARS)) . "&Cognome=" . urlencode(strtr($this->get('surname'), UserStateEnum::UNWANTEND_CHARS))
            . "&ComuneNascita=" . urlencode(strtr($this->get('city_birth'), UserStateEnum::CITY_CHARS))
            . "&DataNascita=" . urlencode($data_di_nascita)
            . "&Sesso=" . $sesso;


        $opts = array('http' => array('header' => "Content-Type: text/xml; charset=utf-8\r\nUser-Agent:Mindwork.it/1.0\r\n"));
        $context = stream_context_create($opts);

        $respond_cf = @simplexml_load_string(file_get_contents($url_check_cf, FALSE, $context));
        if (!$respond_cf) {
            return false;
        }

        if (isset($respond_cf[0])) {
            return trim($respond_cf[0]);
        }

        */

        $codice = $this->cfService->calcola($this->get('name'), $this->get('surname'), $data_di_nascita, $sesso, $this->get('city_birth'), $this->get('province_birth'), $this->get('country_birth'));

        if(!$codice) {
            return false;
        }

        return $codice;
    }

    /*public function checkCodiceFiscale($cf, $checkExists = false)
    {

        if (empty($cf)) {
            return false;
        }

        $url_check_cf = "https://webservices.dotnethell.it/codicefiscale.asmx/ControllaCodiceFiscale?CodiceFiscale=" . $cf;

        $opts = array('http' => array('header' => "Content-Type: text/xml; charset=utf-8\r\nUser-Agent:Mindwork.it/1.0\r\n"));
        $context = stream_context_create($opts);

        $respond_cf = @simplexml_load_string(file_get_contents($url_check_cf, FALSE, $context));
        if (!$respond_cf) {
            return false;
        }

        if (trim($respond_cf[0]) == 'Il codice è valido!') {
            return true;

        }
        return false;
    } */

    public function rules()
    {

        $array = [
            'email' => 'required|email',
            'username' => 'required|unique:users,username|min:8|alpha_num',
            "surname" => 'required',
            "name" => 'required',
            "gender" => 'required',
            "date_birth" => 'required|date_format:Y-m-d',
            "city_birth" => 'required',
            "fiscal_code" => 'required_if:country_birth,!=,Italia'
            //     "country_birth" => 'required'

        ];
        if ($this->get('accept_privacy') == 'false') {
            $array += [
                'accept_privacy_not_false' => 'required',
            ];
        }
        if ($this->get('check_fiscal_code') != 'true') {
            if (strtolower($this->get('country_birth')) == 'italia' && $this->get('fiscal_code')) {

                $get_codice_fisale = $this->getCodiceFiscaleDaDati();
                if (!$get_codice_fisale) {
                    $array += [
                        'fiscal_code_error' => 'required',
                    ];
                }
                /* $check_codice_fiscale=$this->checkCodiceFiscale(trim($this->get('fiscal_code')));
                if (!$check_codice_fiscale) {
                    $array += [
                        'fiscal_code_check' => 'required',
                    ];
                } */
                if(!$get_codice_fisale || strtolower($get_codice_fisale) != strtolower($this->get('fiscal_code'))){
                    $array += [
                        'fiscal_code_not_ugual' => 'required',
                    ];
                }

            }
        }

        if (strtolower($this->get('country_birth')) == 'italia') {
            $array += [
                "province_birth" => 'required',
                "region_birth" => 'required'
            ];
        }

        if (strtolower($this->get('country_residence')) == 'italia') {
            $array += [
                "province_residence" => 'required',
                "region_residence" => 'required',
            ];
        }

        if ($this->get('user_partner') != "false") {
            $array += [
                //'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',
                'cell_phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',
                "country_residence" => 'required',
                "city_residence" => 'required',
                "address_residence" => 'required',
                "country_domicile" => 'required_if:residence_same_domicile,false',
                "city_domicile" => 'required_if:residence_same_domicile,false,false',
                "address_domicile" => 'required_if:residence_same_domicile,false',
                //   "profession" => 'required',
                //  "degree_study" => 'required',
                //"language" => 'required',
                // "document_image" => 'required',
                // "document_consent" => "required",
                // "privacy_module" => "required",
                "accept_privacy_partner" => 'accepted',
                "accept_rule_partner" => 'accepted',
                "accept_statute_partner" => 'accepted',
                "accept_partnerconsent_1" => 'required',
                "accept_partnerconsent_2" => 'required',
                "accept_partnerconsent_3" => 'required',
                "accept_partnerconsent_4" => 'required'
            ];

            if (strtolower($this->get('country_domicile')) == 'italia' && $this->get('residence_same_domicile') != 'true') {
                $array += [
                    "province_domicile" => 'required_if:residence_same_domicile,false',
                    "region_domicile" => 'required_if:residence_same_domicile,false',
                ];
            }

        }

        return $array;

    }

    public function messages()
    {
        return [
            'username.required' => 'Lo username è obbligatorio',
            "surname.required" => "Cognome obbligatorio",
            "name.required" => "Nome obbligatorio",
            "gender.required" => "Sesso obbligatorio",
            "email.required" => "Email obbligatoria",
            "date_birth.required" => "Data di nascita obbligatoria",
            "date_birth.date_format" => "Formato data di nascita errato",
            "city_birth.required" => "Città di nascita obbligatoria",
            'password.required' => 'La password è obbligatorio',
            'password.min' => "La password deve essere di almeno 8 caratteri.",
            'password.regex' => "Almeno 8 caratteri, con almeno 1 numero, 1 lettera maiuscola e 1 carattere speciale.",
            'password.confirmed' => "Password non uguali",
            "fiscal_code_check.required" => "Codice fiscale non valido",
            "document_consent.required" => "Documento obbligatorio",
            "fiscal_code_error.required" => "Controllare i campi per il calcolo del codice fiscale ",
            "fiscal_code_not_ugual.required" => "Il codice fiscale è errato, i campi sono errati ",
            "phone.required" => "Telefono obbligatorio",
            "address_residence.required" => "Indirizzo obbligatorio",
            "document_image.required" => "Documento obbligatorio",
            "privacy_module.required" => "Modulo di richiesta associazione obbligatorio",
            "accept_privacy_not_false.required"=>"Accettare la privacy policy",
            "accept_statute_partner.required"=>"Accettare lo Statuto",
            "accept_rule_partner.required"=>"Accettare il Regolamento di attuazione dello Statuto",
            "accept_privacy_partner.required"=>"Accettare il Regolamento sul funzionamento delle assemblee",
            "email.email" => "Campo email non valido",
            "country_residence.required" => "Nazione Obbligatoria",
            "city_residence.required" => "Città obbligatoria",
            "province_residence.required" => "Provincia obbligatoria",
            "address_residence.required" => "Indirizzo obbligatorio",
            "accept_statute_partner.accepted" => "Accettare lo Statuto",
            "accept_rule_partner.accepted" => "Accettare il Regolamento di attuazione dello Statuto",
            "accept_privacy_partner.accepted" => "Accettare il Regolamento sul funzionamento delle assemblee",
            "accept_partnerconsent_1.required" => "È richiesto di esprimere una preferenza per ognuno dei \"Consensi Soci\" previsti",
            "accept_partnerconsent_2.required" => "È richiesto di esprimere una preferenza per ognuno dei \"Consensi Soci\" previsti",
            "accept_partnerconsent_3.required" => "È richiesto di esprimere una preferenza per ognuno dei \"Consensi Soci\" previsti",
            "accept_partnerconsent_4.required" => "È richiesto di esprimere una preferenza per ognuno dei \"Consensi Soci\" previsti"
        ];
    }
}
