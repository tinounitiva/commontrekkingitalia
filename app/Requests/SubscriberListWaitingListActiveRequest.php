<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
class SubscriberListWaitingListActiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        if($this->type === "waiting" || $this->type === "new"){
            $array = [
                'nome' => 'required',
                'cognome' => 'required',
                'email' => 'required',
            ];
        }

        if($this->type === "reserved"){
            $array = [
                'nome' => 'required',
                'cognome' => 'required',
                'email' => 'required',
                'data_scadenza' => 'required',
            ];
        }

        return $array;
    }

    public function messages()
    {
        return [
            'nome.required' => 'Nome è obbligatorio',
            'cognome.required' => 'Cognome è obbligatorio',
            'email.required' => 'Email è obbligatorio',
            'data_scadenza.required' => 'Data di scadenza è obbligatorio',
        ];
    }
}
