<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Support\Facades\Hash;
use TrekkingItalia\Common\Models\User;

class DepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required',
            'region_residence_id' => 'required',
            'municipality_residence_id' => 'required',
            'province_residence_id' => 'required',
            'email_principal' => 'required|email',
            'email_administrative' => 'required|email',
            'email_info' => 'required|email',
        ];
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function messages()
    {
        return [
            'name.required' => 'Il nome è richiesto.',
            'region_residence_id.required' => 'La regione è richiesta.',
            'municipality_residence_id.required' => 'È richiesto il Comune.',
            'province_residence_id.required' => 'La provincia è richiesta.',
            'email_principal.required' => "È richiesto l'indirizzo email principale.",
             "email_administrative.required" => "È richiesto l'amministratore dell'e-mail.",
             "email_info.required" => 'Le informazioni e-mail sono richieste.',
             "email_principal.email" => "L'entità email deve essere un indirizzo email valido.",
             'email_administrative.email' => "L'amministratore dell'email deve essere un indirizzo email valido.",
             'email_info.email' => "Le informazioni e-mail devono essere un indirizzo e-mail valido.",
        ];
    }

}
