<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class TrekBilanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            'min_partecipanti' => 'required',
            'contributo_partecipanti' => 'required',
            'soci_accompagnotori' => 'required',
            'contributo_soci' => 'required',
            'partecipanti' => 'required',
            'quota_trek_definita' => 'required',
        ];


    }

    public function messages()
    {
        return [
            'min_partecipanti.required' => 'Numero Partecipanti Minimo è obbligatorio',
            'contributo_partecipanti.required' => 'Contributo partecipanti obbligatorio',
            'soci_accompagnotori.required' => 'oci accompagnaotiri obbligatoria',
            'contributo_soci.required' => 'Contributo soci è obbligatoria',
            'partecipanti.required' => 'Partecipanti obbligatorii',
            'quota_trek_definita.required' =>'Quota trek definitiva obbligatoria',
        ];
    }
}
