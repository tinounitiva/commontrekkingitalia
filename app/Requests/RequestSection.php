<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestSection extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {


        $array = [
            'title' => 'required',
            'page' => 'required',
            'template' => 'required',
            'status' => 'required',
        ];


        return $array;
    }

    public function messages()
    {
        return [
            'title.required' => 'Titolo obbligatorio',
            'page.required' => 'number_page obbligatorio',
            'template.required' => 'Template obbligatorio',
            'status.required' => 'Stato obbligatorio',
        ];
    }
}
