<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Support\Facades\Hash;
use TrekkingItalia\Common\Models\User;

class CreateMembershipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'department_id' => 'required',
            'association_type_id' => 'required',
            'price' => 'required',
            'payment_method_id' => 'required', //channel_id
            'department_credit_id' => 'required',
            "date_start"=>"required"
        ];
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function messages()
    {
        return [
            'department_id.required' => 'La sezione è obbligatoria.',
            'association_type_id.required' => 'Il tipo di associazione è obbligatorio.',
            'price.required' => 'La quota è obbligatoria.',
            'payment_method_id.required' => 'La modalità di pagamento è obbligatoria.',
            'department_credit_id.required' => 'La linea di credito è obbligatoria.',
            "date_start.required"=>"La data è obbligatoria"

        ];
    }

}
