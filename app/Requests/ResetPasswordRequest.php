<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Support\Facades\Hash;
use TrekkingItalia\Common\Models\User;
use TrekkingItalia\Common\Helpers\TrekkingItaliaHelper;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $array = [
            'token' => 'required',
            // 'email' => 'required|email',
            'username' => ['required']
        ];
        if (TrekkingItaliaHelper::isDev()) {
            $array += [
                'password' => 'required|confirmed'
            ];
        } else {
            $array += [
                'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'
            ];
        }

        if($this->get('first_login')) {
            if(!$this->get('accept_privacy') || $this->get('accept_privacy') == 'false') {
                $array += [
                    'accept_privacy_not_false' => 'required',
                ];
            }
            $user = User::where('username', $this->get('username'))->first();

            if($user && $user->user_partner_state == '1') {
                if(!$this->get('accept_privacy_partner') || $this->get('accept_privacy_partner') == 'false') {
                    $array += [
                        'accept_privacy_partner_not_false' => 'required',
                    ];
                }
                if(!$this->get('accept_rule_partner') || $this->get('accept_rule_partner') == 'false') {
                    $array += [
                        'accept_rule_partner_not_false' => 'required',
                    ];
                }
                if(!$this->get('accept_statute_partner') || $this->get('accept_statute_partner') == 'false') {
                    $array += [
                        'accept_statute_partner_not_false' => 'required',
                    ];
                }
                $array += [
                    "accept_partnerconsent_1" => 'required',
                    "accept_partnerconsent_2" => 'required',
                    "accept_partnerconsent_3" => 'required',
                    "accept_partnerconsent_4" => 'required'
                ];
            }
        }

        return $array;
    }

    public function messages()
    {
        return [
            'username.required' => 'Il nome utente è richiesto.',
            'password.required' => 'La password è richiesta.',
            'password.min' => "La password deve contenere almeno 8 caratteri.",
            'password.regex' => "La password deve contenere almeno 1 numero, 1 lettera maiuscola, 1 lettera minuscola, e 1 carattere speciale.",
            'password.confirmed' => "La password inserita non coincide.",
            "accept_privacy_not_false.required" => "Accettare la Privacy Policy",
            "accept_statute_partner_not_false.required" => "Accettare lo Statuto",
            "accept_rule_partner_not_false.required" => "Accettare il Regolamento di attuazione dello Statuto",
            "accept_privacy_partner_not_false.required" => "Accettare il Regolamento sul funzionamento delle assemblee",
            "accept_partnerconsent_1.required" => "È richiesto di esprimere una preferenza per ognuno dei \"Consensi Soci\" previsti",
            "accept_partnerconsent_2.required" => "È richiesto di esprimere una preferenza per ognuno dei \"Consensi Soci\" previsti",
            "accept_partnerconsent_3.required" => "È richiesto di esprimere una preferenza per ognuno dei \"Consensi Soci\" previsti",
            "accept_partnerconsent_4.required" => "È richiesto di esprimere una preferenza per ognuno dei \"Consensi Soci\" previsti"
        ];
    }


}
