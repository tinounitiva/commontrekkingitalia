<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
class TrekDailyProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
/*
PROGRAMA GIRONALIRA
Tipo di giornata (campo select, obbligatorio,
 Descrizione giornata (textarea WYSIWYG, obbligatoria, default vuota)



Se il tipo di giornata è uguale a “trek”, saranno presenti in più i seguenti campi:
o Località di partenza (input text, obbligatorio, default vuoto)
o Un box di OpenStreetMap per identificare un punto sulla mappa: da tale
punto, verranno estrapolati latitudine e longitudine della località di partenza
o Altitudine di partenza (metri) (input number, obbligatorio, default vuoto)
o Località di arrivo (input text, obbligatorio, default vuoto)
o Un box di OpenStreetMap per identificare un punto sulla mappa: da tale
punto, verranno estrapolati latitudine e longitudine della località di arrivo


o Altitudine di arrivo (metri) (input number, obbligatorio, default vuoto)
o Lunghezza percorso (km) (input number, obbligatorio, default vuoto)
o Tempo totale cammino (input time “hh:mm”, obbligatorio, default vuoto)
o Dislivello salita (metri) (input number, obbligatorio, default vuoto)
o Dislivello discesa (metri) (input number, obbligatorio, default vuoto)
o Altitudine minima (metri) (input number, obbligatorio, default vuoto)
o Altitudine massima (metri) (input number, obbligatorio, default vuoto)

*/
        $array = [
            'type_day' => 'required',
            'trek_id' => 'required',
            'description' => 'required',
            "locality_start" => 'required_if:type_day,==,trek',
            /*"long_start" => 'required_if:type_day,==,trek',
            "lat_start" => 'required_if:type_day,==,trek',*/
            "alt_start" => 'required_if:type_day,==,trek',
            "locality_end" => 'required_if:type_day,==,trek',
           /* "long_end" => 'required_if:type_day,==,trek',
            "lat_end" => 'required_if:type_day,==,trek',*/
            "alt_end" => 'required_if:type_day,==,trek',
            "path_length" => 'required_if:type_day,==,trek',
            "total_time" => 'required_if:type_day,==,trek',
            "ascent_difference" => 'required_if:type_day,==,trek',
            "descent_difference" => 'required_if:type_day,==,trek',
            "altitude_min" => 'required_if:type_day,==,trek',
            "altitude_max" => 'required_if:type_day,==,trek',
        ];


        return $array;
    }

    public function messages()
    {
        return [
            'title.required' => 'Titolo è obbligatorio',
            'trek_id.required' => 'Trek è obbligatorio',
            'description.required' => 'Descrizione è obbligatorio',
            'locality_start.required' => ' Località di partenza è obbligatorio',
            /*'long_start.required' => 'latitudine è obbligatorio',
            'lat_start.required' => 'longitudine è obbligatorio',*/
            'alt_start.required' => 'Altitudine di partenza è obbligatorio',
            'locality_end.required' => 'Località di arrivo  è obbligatorio',
            /*'long_end.required' => 'latitudine è obbligatorio',
            'lat_end.required' => 'longitudine è obbligatorio',*/
            'alt_end.required' => ' Altitudine di arrivo  è obbligatorio',
            'path_length.required' => 'Lunghezza percorso (km)  è obbligatorio',
            'total_time.required' => 'Tempo totale cammino è obbligatorio',
            'ascent_difference.required' => ' Dislivello salita (metri)  è obbligatorio',
            'descent_difference.required' => 'Dislivello discesa (metri)  è obbligatorio',
            'altitude_min.required' => 'Altitudine minima (metri)  è obbligatorio',
            'altitude_max.required' => ' Altitudine massima (metri) è obbligatorio',
        ];
    }
}
