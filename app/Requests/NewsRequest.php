<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        $array = [
            'title' => 'required',
            'category_trek_id' => 'required',
            'department_id' => 'required',
            //    'published_at' => 'required|date_format:Y-m-d H:i:s|date|after_or_equal:today',


        ];
        if ($this->get('published_at')) {

            $date1 = Carbon::parse($this->get('published_at'))->tz('Europe/Rome');

            $date2 = Carbon::now();


//            if ($date1->lte($date2)) {
//                $array += [
//                    'date_not_correct_today' => 'required',
//                ];
//            }

        }else{
            $array += [
                'date_not_published_at_present' => 'required',
            ];
        }
        if ($this->get('expiration_at')) {
            $date1 = Carbon::parse($this->get('published_at'))->tz('Europe/Rome');

            $date2 = Carbon::parse($this->get('expiration_at'))->tz('Europe/Rome');


            if (!$date1->lte($date2)) {
                $array += [
                    'date_not_correct' => 'required',
                ];
            }

        }
        return $array;

    }

    public function messages()
    {
        return [
            'title.required' => 'Titolo è obbligatorio',
            'category_trek_id.required' => 'Categoria trek è  obbligatorio',
            'department_id.required' => 'La sezione è obbligatoria',
            'published_at.required' => 'La data di pubblicazione è obbligatoria',
            'published_at.date_format' => 'Il formato è errato deve esser Y-m-d H:i:s',
            'published_at.after_or_equal' => 'La data deve essere maggiore ad oggi',
            'date_not_correct.required' => "La data di pubblicazione è inferiore alla data di scadenza",
            'date_not_correct_today.required' => "La data di pubblicazione è inferiore alla data di oggi",
            "date_not_published_at_present"=>"La data di inizio non è presente",
        ];
    }
}
