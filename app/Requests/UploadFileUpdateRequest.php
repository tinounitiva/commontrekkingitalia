<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Support\Facades\Hash;
use TrekkingItalia\Common\Models\User;
use TrekkingItalia\Common\Helpers\TrekkingItaliaHelper;

class UploadFileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titolo' => 'required',
          //  'active' => 'required',
        ];

    }
    public function messages()
    {
        return [
            'titolo.required' => 'Il titolo è richiesto',
          //  'active.required' => 'required',
        ];
    }


}
