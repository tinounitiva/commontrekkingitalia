<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class CourseCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {

        $array = [
            'department' => 'required',
            'title' => 'required',
            "visible_public_site" => 'required',
            "total_hours" => 'required',
            "number_meets" => 'required',
            "locality" => 'required',
        ];

        return $array;

    }

    public function messages()
    {
        return [
            'department.required' => 'Sezione obbligatoria',
            "title.required" => "Titolo obbligatorio",
            "visible_public_site.required" => "Visibilità obbligatorio",
            "total_hours.required" => "Totale ore corso obbligatorio",
            "number_meets.required" => "Numero di incontri obbligatoria",
            "locality.required" => "Denominazione della località di effettuazione del corso",

        ];
    }
}
