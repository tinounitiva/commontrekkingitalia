<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class TrekQuoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        //   'closing_date.required' => 'Data chiusura iscrizione obbligatoria',
        // 'date_last_payment.required' => 'Data ultimo avviso pagamento obbligatoria',
        // 'last_communication.required' => 'Data ultima comunicazione ai partecipanti obbligatoria',
        // 'balance_payment_deadline.required' => 'Data ultimo pagamento saldo obbligatoria',
        // 'last_date_trek.required' => 'Data ultima per formazione obbligatoria',

        $array = [
            'closing_date' => 'required',
            'last_communication' => 'required',
            //'balance_payment_deadline' => 'required',
            //'date_last_payment' => 'required',
            'last_date_trek' => 'required',
            'trek_id' => 'required',
            'participants_max' => 'required',
            'reserved_seat' => 'required',
            'participation_quote' => 'required',

        ];
        if ($this->get('participation_quote') == "variable")
            $array += [
                'quote_min' => 'required',
                'quote_max' => 'required',
            ];

        if ($this->get('participation_quote') != 'free') {
            $array += [
                'management_quote' => 'required',
            ];
        }
        if ($this->get('management_quote') == 'advance_balance') {
            if (!$this->get('advance_quote')) {
                $array += [
                    'advance_quote' => 'required',
                ];
            } else {
                $quota_norma = $this->get('quote') ?? 100;
                $balance_quote = $this->get('balance_quote');
                $tot = $quota_norma - $balance_quote;

                if($this->get('advance_quote')>$tot){
                    $array += [
                        'advance_quote_max' => 'required',
                    ];
                }

            }
        }
        if ($this->get('management_quote') != 'free') {

            if (!$this->get('balance_quote')) {
                $array += [
                    'balance_quote' => 'required',
                ];
            } else {
                if($this->get('management_quote') == 'balance'){
                    $quota_norma = $this->get('quote') ?? 100;
                    $advance_quote = $this->get('advance_quote');
                    $tot = $quota_norma - $advance_quote;

                    if($this->get('balance_quote')>$tot){
                        $array += [
                            'balance_quote_max_anticipo' => 'required',
                        ];
                    }
                }
                if($this->get('management_quote') != 'advance_balance'){
                    $quota_norma = $this->get('quote') ?? 100;
                    if($this->get('advance_quote')>$quota_norma){
                        $array += [
                            'balance_quote_max' => 'required',
                        ];
                    }
                }
            }
        }
        if ($this->get('early_booking') && !$this->get('expiration_date'))
            $array += [
                'expiration_date' => 'required',
            ];

        return $array;

    }

    public function messages()
    {
        return [
            'closing_date.required' => 'Data chiusura iscrizione obbligatoria',
            'date_last_payment.required' => 'Data ultimo avviso pagamento obbligatoria',
            'last_communication.required' => 'Data ultima comunicazione ai partecipanti obbligatoria',
            'balance_payment_deadline.required' => 'Data ultimo pagamento saldo obbligatoria',
            'last_date_trek.required' => 'Data ultima per formazione obbligatoria',
            'trek_id.required' => 'Trek id obbligatorio',
            'participants_max.required' => 'Partecipanti massimi  obbligatoria',
            'reserved_seat.required' => 'Posti prenotabili obbligatoria',
            'participation_quote.required' => 'Tipo quota di partecipazione destinazione obbligatoria',
            'management_quote.required' => 'Gestione quota obbligatoria',
            'quote_min.required' => 'Quota Minimo obbligatoria',
            'quote_max.required' => 'Quota Massima  obbligatoria',
            'advance_quote.required' => 'Quota anticipo obbligatori',
            'balance_quote.required' => 'Quota Saldo obbligatori',
            'advance_quote_max.required' => 'Il valore non potrà essere maggiore della differenza fra la quota normale e la quota saldo',
            'balance_quote_max.required' => 'In caso di “solo saldo”, non dovrà essere maggiore della quota normale',
            'balance_quote_max_anticipo.required' => 'In caso di “solo saldo”, non dovrà essere maggiore della quota normale',
            'expiration_date.required' => 'Data di scadenza obbligatoria',
            'note.required' => 'Campo note obbligatorio obbligatoria',
        ];
    }
}
