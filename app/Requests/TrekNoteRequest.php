<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
class TrekNoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        $array = [
            'trek_id' => 'required',
            'description' => 'required',
            'info_start' => 'required',
            'info_end' => 'required',

        ];


        return $array;
    }

    public function messages()
    {
        return [
            'trek_id.required' => 'Trek è obbligatorio',
            'description.required' => 'Descrizione è obbligatoria',
            'info_start.required' => ' Info viaggio andata ',
            'info_end.required' => 'Info viaggio ritorno ',
        ];
    }
}
