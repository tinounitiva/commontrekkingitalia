<?php

namespace TrekkingItalia\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class TrekRouteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {


        $array = [
            'countries' => 'required',
            'trek_id' => 'required',
            'name_start' => 'required',
            'name_end' => 'required',
            'locality' => 'required',
            //'latitude_start' => 'required',
            //'longitude_start' => 'required',
            'latitude_end' => 'required',
            'longitude_end' => 'required',
        ];


        return $array;

    }

    public function messages()
    {
        return [
            'countries.required' => 'Nazione obbligatoria',
            'locality.required' => 'Nome della località di ritrovo obbligatorio',
            'name_start.required' => 'Nome della località di partenza obbligatorio',
            'name_end.required' => 'Nome della destinazione obbligatoria',
            'latitude_start.required' => 'Latitudine località di partenza obbligatoria',
            'longitude_start.required' => 'Longitudine  località di partenza obbligatoria',
            'latitude_end.required' => 'Longitudine  della destinazione obbligatoria',
            'longitude_end.required' => 'Longitudine  della destinazione obbligatoria',
        ];
    }
}
