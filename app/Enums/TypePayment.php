<?php

namespace TrekkingItalia\Common\Enums;

class TypePayment {

    public const CARICABORSELLINO = 'Versamento';
    public const RIMBORSO = 'Richiesta rimborso';
    public const ATTIVAZIONEWALLET = 'Attivazione Wallet';
    public const DISATTIVAZIONEWALLET = 'Disattivazione Wallet';
    public const PAGAMENTO = 'Pagamento';
    public const QUOTASSOCIATIVA = 'Rinnovo quota associativa';
    public const RESTITUZIONE = 'Restituzione quota';

}