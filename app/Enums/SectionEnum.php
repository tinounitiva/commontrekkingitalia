<?php

namespace TrekkingItalia\Common\Enums;

class SectionEnum
{

    public const BOZZA= 'BOZZA';
    public const APPROVATA = 'APPROVATA';
    public const INATTESA= 'IN_ATTESA_APPROVAZIONE';


    public const DISATTIVA = 'DISATTIVA';
    public const ATTIVA= 'ATTIVA';

    public const TEMPLATE_TEXT = 'text';
    public const TEMPLATE_TEXT_GALLERY= 'text_galley';
    public const TEMPLATE_TEXT_IMG= 'text_img';

    public const IMAGE_SFONDO= 'IMAGE_SFONDO';
    public const TEXT_GALLERY= 'TEXT_GALLERY';
    public const THUMB1= 'THUMB1';
    public const THUMB2= 'THUMB2';
    public const THUMB3= 'THUMB3';
    public const THUMB4= 'THUMB4';
    public const SECONDIMG= 'SECONDIMG';

}
