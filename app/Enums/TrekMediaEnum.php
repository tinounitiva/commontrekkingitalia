<?php

namespace TrekkingItalia\Common\Enums;

class TrekMediaEnum {

    public const CARD = 'card';
    public const NEWSPAPER = 'newspaper';
    public const GENERAL = 'general';
    public const DOCUMENT = 'document';
    public const DAILY = 'daily';

}