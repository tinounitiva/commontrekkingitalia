<?php

namespace TrekkingItalia\Common\Enums;

class NotificationEnum
{
    /*Ricezione di una richiesta di nuova associazione. In questo caso, la notifica arriverà
    a tutti gli Addetti che hanno i permessi necessari per poter approvare o rifiutare tale
    nuova richiesta di associazione (principalmente saranno gli Addetti con permessi
    sulla Sezione di appartenenza di tale ipotetico nuovo Socio)*/

    public const REQUEST_ASSOCIATE = 'REQUEST_ASSOCIATE';


    public const SEND_APPROVATION_TREK_MASTER = 'SEND_APPROVATION_TREK_MASTER';
    public const SEND_APPROVATION_TREK_SCHEDULATION = 'SEND_APPROVATION_TREK_SCHEDULATION';
    public const SEND_FIRST_APPROVATION_TREK_MASTER = 'SEND_FIRST_APPROVATION_TREK_MASTER';
    public const SEND_FIRST_APPROVATION_TREK_SCHEDULATION = 'SEND_FIRST_APPROVATION_TREK_SCHEDULATION';
    public const SEND_FINAL_APPROVATION_TREK_MASTER = 'APPROVATION_TREK_MASTER';
    public const SEND_FINAL_APPROVATION_TREK_SCHEDULATION = 'APPROVATION_TREK_SCHEDULATION';
    public const SEND_FINAL_APPROVATION_COURSE = 'APPROVATION_COURSE';
    public const SEND_FINAL_APPROVATION_EVENT = 'APPROVATION_EVENT';
    public const SEND_APPROVATION_COURSE = 'SEND_APPROVATION_COURSE';
    public const SEND_APPROVATION_EVENT = 'SEND_APPROVATION_EVENT';
    public const REFUSED_TREK = 'REFUSED_TREK';
    public const REFUSED_COURSE = 'REFUSED_COURSE';
    public const REFUSED_EVENT = 'REFUSED_EVENT';
    public const QUESTION_SITE_PUBLIC = 'QUESTION_SITE_PUBLIC';


    public const  SEND_TO_APPROVATION_SECTION='SEND_TO_APPROVATION_SECTION';
    public const  SEND_APPROVATION_SECTION='SEND_APPROVATION_SECTION';
    public const  REFUSED_SECTION='REFUSED_SECTION';

}