<?php

namespace TrekkingItalia\Common\Enums;

class TypePriceEnum
{

    public const NOTVISIBLE = 'Non visibile all’utente';
    public const  FIXED = 'Importo fisso';
    public const  LEAVINGFROM = 'Importo a partire da';

    public const FISSO = 'importo_fisso';
    public const PARTIREDA = 'importo_a_partire_da';
    public const NONVISIBILE = 'non_visibile';

}
