<?php

namespace TrekkingItalia\Common\Enums;

class TrekDailyProgramEnum {

    public const TRANSFERT = 'trasferimento';
    public const TREK = 'trek';
    public const FREEDATA = 'giornata_libera';

}