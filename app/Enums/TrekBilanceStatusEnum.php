<?php

namespace TrekkingItalia\Common\Enums;

class TrekBilanceStatusEnum {

    public const OPEN = 'open';
    public const CLOSE = 'close';

}