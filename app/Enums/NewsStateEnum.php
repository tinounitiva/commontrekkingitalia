<?php

namespace TrekkingItalia\Common\Enums;

class NewsStateEnum {

    public const ACTIVE = 'active';
    public const DISABLED = 'disabled';

}