<?php

namespace TrekkingItalia\Common\Enums;

class TrekMagazineStateEnum {

    public const ACTIVE = 'active';
    public const DISABLED = 'disabled';

}