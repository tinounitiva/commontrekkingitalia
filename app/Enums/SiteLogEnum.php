<?php

namespace TrekkingItalia\Common\Enums;

class SiteLogEnum {

    public const CODE = array(
        'LOGIN1' => array(
            'id' => 'LOGIN1',
            'description' => 'Login al sito pubblico',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'LOGOUT1' => array(
            'id' => 'LOGOUT1',
            'description' => 'Logout al sito pubblico',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'LOGIN' => array(
            'id' => 'LOGIN',
            'description' => 'Login al gestionale',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'LOGOUT' => array(
            'id' => 'LOGOUT',
            'description' => 'Logout al gestionale',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'ISCRTK1' => array(
            'id' => 'ISCRTK1',
            'description' => 'Iscrizione al trek dal sito pubblico',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'PAGTK1' => array(
            'id' => 'PAGTK1',
            'description' => 'Pagamento quota trek da sito pubblico',
            'model_type' => 'subscriber_management',
            'model_id' => 'id'
        ),
        'ISCLA1' => array(
            'id' => 'ISCLA1',
            'description' => 'Iscrizione lista attesa da sito pubblico',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'ISCRCS1' => array(
            'id' => 'ISCRCS1',
            'description' => 'Iscrizione al corso da sito pubblico',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'PAGCS1' => array(
            'id' => 'PAGCS1',
            'description' => 'Pagamento quota corso da sito pubblico',
            'model_type' => 'subscriber_management',
            'model_id' => 'id'
        ),
        'ISCREV1' => array(
            'id' => 'ISCREV1',
            'description' => 'Iscrizione all\'evento da sito pubblico',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'PAGEV1' => array(
            'id' => 'PAGEV1',
            'description' => 'Pagamento quota evento da sito pubblico',
            'model_type' => 'subscriber_management',
            'model_id' => 'id'
        ),
        'RICAS1' => array(
            'id' => 'RICAS1',
            'description' => 'Richiesta associazione da sito pubblico',
            'model_type' => 'partnership_requests',
            'model_id' => 'id'
        ),
        'RICUT1' => array(
            'id' => 'RICUT1',
            'description' => 'Richiesta utenza da sito pubblico',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'PAGQA1' => array(
            'id' => 'PAGQA1',
            'description' => 'Pagamento quota associativa da sito pubblico',
            'model_type' => 'memberships',
            'model_id' => 'id'
        ),
        'MODPW1' => array(
            'id' => 'MODPW1',
            'description' => 'Modifica password',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'RICAS' => array(
            'id' => 'RICAS',
            'description' => 'Inserimento richiesta associazione',
            'model_type' => 'partnership_requests',
            'model_id' => 'id'
        ),
        'RICUT' => array(
            'id' => 'RICUT',
            'description' => 'Inserimento richiesta utenza',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'APPSOC' => array(
            'id' => 'APPSOC',
            'description' => 'Approvazione richiesta associazione',
            'model_type' => 'partnership_requests',
            'model_id' => 'id'
        ),
        'MODANA' => array(
            'id' => 'MODANA',
            'description' => 'Modifica anagrafica socio',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'ELIANA' => array(
            'id' => 'ELIANA',
            'description' => 'Eliminazione anagrafica socio',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'ELIANAU' => array(
            'id' => 'ELIANAU',
            'description' => 'Eliminazione anagrafica socio con accorpamento',
            'model_type' => 'users',
            'model_id' => 'id'
        ),
        'PAGQA' => array(
            'id' => 'PAGQA',
            'description' => 'Registrazione pagamento quota associativa',
            'model_type' => 'memberships',
            'model_id' => 'id'
        ),
        'PAGQAA' => array(
            'id' => 'PAGQAA',
            'description' => 'Annullamento pagamento quota associativa',
            'model_type' => 'memberships',
            'model_id' => 'id'
        ),
        'INSMAS' => array(
            'id' => 'INSMAS',
            'description' => 'Inserimento scheda master',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'MODMAS' => array(
            'id' => 'MODMAS',
            'description' => 'Modifica scheda master',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'APPMAS' => array(
            'id' => 'APPMAS',
            'description' => 'Approvazione scheda master',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'RBMAS' => array(
            'id' => 'RBMAS',
            'description' => 'Ritorno bozza scheda master',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'ELIMAS' => array(
            'id' => 'ELIMAS',
            'description' => 'Eliminazione scheda master',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'NVMSCH' => array(
            'id' => 'NVMSCH',
            'description' => 'Nuova schedulazione da scheda master',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'NVSSCH' => array(
            'id' => 'NVSSCH',
            'description' => 'Nuova schedulazione da duplicazione schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'MODSCH' => array(
            'id' => 'MODSCH',
            'description' => 'Modifica schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'ELISCH' => array(
            'id' => 'ELISCH',
            'description' => 'Eliminazione schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'APPSCH' => array(
            'id' => 'APPSCH',
            'description' => 'Approvazione schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'RBSCH' => array(
            'id' => 'RBSCH',
            'description' => 'Ritorno a bozza schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'PUBSCH' => array(
            'id' => 'PUBSCH',
            'description' => 'Pubblicazione schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'ANNSCH' => array(
            'id' => 'ANNSCH',
            'description' => 'Annullamento schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'OFFSCH' => array(
            'id' => 'OFFSCH',
            'description' => 'Passaggio schedulazione da anteprima a Offerta',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'OPNSCH' => array(
            'id' => 'OPNSCH',
            'description' => 'Riapertura contabile schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'CLSSCH' => array(
            'id' => 'CLSSCH',
            'description' => 'Chiusura contabile schedulazione',
            'model_type' => 'treks',
            'model_id' => 'id'
        ),
        'ISCSCH' => array(
            'id' => 'ISCSCH',
            'description' => 'Inserimento Iscrizione Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'PRESCH' => array(
            'id' => 'PRESCH',
            'description' => 'Inserimento Prenotazione Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'LATSCH' => array(
            'id' => 'LATSCH',
            'description' => 'Inserimento lista attesa Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'ISCSCHA' => array(
            'id' => 'ISCSCHA',
            'description' => 'Annullamento Iscrizione Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'PRESCHA' => array(
            'id' => 'PRESCHA',
            'description' => 'Annullamento Prenotazione Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'LATSCHA' => array(
            'id' => 'LATSCHA',
            'description' => 'Annullamento lista attesa Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'ISCSCHE' => array(
            'id' => 'ISCSCHE',
            'description' => 'Eliminazione Iscrizione Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'PRESCHE' => array(
            'id' => 'PRESCHE',
            'description' => 'Eliminazione Prenotazione Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'LATSCHE' => array(
            'id' => 'LATSCHE',
            'description' => 'Eliminazione lista attesa Schedulazione',
            'model_type' => 'subscriber_list_waiting_list_active',
            'model_id' => 'id'
        ),
        'PAGSCH' => array(
            'id' => 'PAGSCH',
            'description' => 'Pagamento quota schedulazione',
            'model_type' => 'subscriber_management',
            'model_id' => 'id'
        ),
        'RESSCH' => array(
            'id' => 'RESSCH',
            'description' => 'Restituzione quota trek',
            'model_type' => 'subscriber_management',
            'model_id' => 'id'
        ),
        'PAGSCHA' => array(
            'id' => 'PAGSCHA',
            'description' => 'Eliminazione Pagamento quota schedulazione',
            'model_type' => 'subscriber_management',
            'model_id' => 'id'
        ),
        'RESSCHA' => array(
            'id' => 'RESSCHA',
            'description' => 'Eliminazione Restituzione quota trek',
            'model_type' => 'subscriber_management',
            'model_id' => 'id'
        )
    );
}