<?php

namespace TrekkingItalia\Common\Enums;

class OpeningStatus {

    public const ANTEPRIMA = 'anteprima';
    public const OFFERTA = 'offerta';
}