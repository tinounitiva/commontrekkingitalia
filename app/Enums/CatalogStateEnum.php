<?php

namespace TrekkingItalia\Common\Enums;

class CatalogStateEnum {

    public const ACTIVE = 'active';
    public const DISABLED = 'disabled';

}