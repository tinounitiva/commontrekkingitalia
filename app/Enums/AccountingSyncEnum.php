<?php

namespace TrekkingItalia\Common\Enums;

class AccountingSyncEnum {

    public const ESERCIZICHIUSURA="T-ESE";// esercizi e date chiusura aggiornamen
    public const COSTISOSTENUTI="T-COS";// cos sostenu per i trek
    public const NOMINATIVISOCI="T-SOC";// nominavi soci
    public const SCHEDULAZIONI="T-SCH";// schedulazioni
    public const QUOTEASSOCIAZIONI="T-QA";// quote associazioni
    public const ISCRIZIONETREK="T-QT";//quote iscrizione trek
    public const ISCRIZIONECORSI="T-QC";// quote iscrizione corsi
    public const ISCRIZIONEEVENTI="T-QE";//quote even
    public const RICARICHEBORSELLINO="T-RB";// ricariche borsellino


    public const STATUSGESTIONALE="GESTIONALE";// il file viene generato dal gestionale
    public const STATUSCONTABILITA="CONTABILITA";// il file viene ricevuto dalla contabilità

//status DI INVIO da utilizzare durante il cron
    public const PENDING="PENDING";//
    public const PROCESSING="PROCESSING";//
    public const DONE="DONE";//
    public const ERROR="ERROR";//
}