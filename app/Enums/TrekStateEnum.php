<?php

namespace TrekkingItalia\Common\Enums;

class TrekStateEnum {

    public const ACTIVE = 'active';
    public const DISABLED = 'disabled';
    public const REVIEW = 'review';

    public const DRAFT = 'draft';
    public const UNDER_APPROVAL = 'under_approval';
    public const APPROVED = 'approved';
    public const REFUSED= 'refused';
    public const CANCELED= 'canceled';

}