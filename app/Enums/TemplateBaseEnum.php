<?php

namespace TrekkingItalia\Common\Enums;

class TemplateBaseEnum {

    public const GALLERY = 'gallery_image';
    public const VIDEO = 'with_video';
    public const CLASSIC = 'classic';

}