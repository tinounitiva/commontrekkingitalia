<?php

namespace TrekkingItalia\Common\Enums;

class UserStatePartnerEnum {

    public const PENDING = 'pending';
    public const ACCEPTED = 'accepted';
    public const REFUSED = 'refused';

}
