<?php

namespace TrekkingItalia\Common\Enums;

class EventsStateEnum {


    public const DRAFT = 'draft';
    public const UNDER_APPROVAL = 'under_approval';
    public const APPROVED = 'approved';
    public const REFUSED= 'refused';
    public const HISTORY= 'history';

}