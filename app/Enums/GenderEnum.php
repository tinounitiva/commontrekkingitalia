<?php

namespace TrekkingItalia\Common\Enums;

class GenderEnum
{

    public const UOMO = 'uomo';
    public const  DONNA = 'donna';
    public const  ALTRO = 'altro';


}
