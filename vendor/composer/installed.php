<?php return array(
    'root' => array(
        'name' => 'trekkingitalia/common',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '99c96ab3b04863830fe2570faac3dc48049002e8',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'trekkingitalia/common' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '99c96ab3b04863830fe2570faac3dc48049002e8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
